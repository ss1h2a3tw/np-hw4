CXX := g++

CXX_FLAGS += -O2 -Wall -Wextra -std=c++14 -lpthread

SRC_FILES := main.cpp

TARGET := main

.phony: all

all: $(TARGET) hw4.cgi

$(TARGET): $(SRC_FILES)
	$(CXX) $(CXX_FLAGS) $(SRC_FILES) -o $(TARGET)

hw4.cgi: cgi.cpp
	$(CXX) $(CXX_FLAGS) cgi.cpp -o hw4.cgi

