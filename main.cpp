#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>
#include <map>
#include <sstream>
#include <fstream>
#include <istream>
#include <vector>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <thread>

using namespace std;

int listenfd;
const size_t readbuf_len = 65536;
char readbuf[readbuf_len];
uint32_t allowid;
uint32_t allowmask;
uint32_t cli_allowid;
uint32_t cli_allowmask;

struct __attribute__((packed)) request{
    uint8_t vn;
    uint8_t cd;
    uint16_t dstport;
    uint32_t dstip;
    char userid[1];
};
struct __attribute__((packed)) reply{
    uint8_t vn;
    uint8_t cd;
    uint16_t dstport;
    uint32_t dstip;
};

void proxy(int readfd,int writefd){
    vector<uint8_t> buf(readbuf_len);
    try{
        while(1){
            auto size=read(readfd,buf.data(),buf.size());
            if(size==0){
                shutdown(readfd,SHUT_RD);
                shutdown(writefd,SHUT_WR);
                return;
            }
            if(size==-1)throw("Failed in reading data");
            cerr << "Readed " << size << " bytes First Byte: "<< hex << (unsigned int)buf[0] << dec << endl;
            size_t writedsize=0;
            while(writedsize<(size_t)size){
                auto wsize=write(writefd,buf.data()+writedsize,size-writedsize);
                if(wsize<=0)throw("Failed in write");
                writedsize+=wsize;
            }
        }
    }
    catch(const char* err){
        cerr << err << endl;
    }
}

int setuplistensocket(size_t port){
    int fd;
    fd=socket(AF_INET,SOCK_STREAM,0);
    if(fd==-1)throw("Error opening socket");
    const int enable = 1;
    if(setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(enable))==-1)throw("Error setting socket");
    sockaddr_in addr;
    addr.sin_family=AF_INET;
    addr.sin_port=htons(port);
    addr.sin_addr.s_addr=INADDR_ANY;
    if(bind(fd,(sockaddr*)&addr,sizeof(sockaddr_in))==-1)throw("Failed to bind socket");
    if(listen(fd,10)==-1)throw("Failed to listen");
    return fd;
}

void processconnect(const request& req,int clifd){
    reply rep;
    rep.vn=0;
    rep.cd=90;
    rep.dstport=req.dstport;
    rep.dstip=req.dstip;
    int dstfd=socket(AF_INET,SOCK_STREAM,0);
    if(dstfd==-1)throw("Failed to create socket");
    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = req.dstport;
    addr.sin_addr.s_addr=req.dstip;
    cerr << "Connecting: " << inet_ntoa(addr.sin_addr) << endl;
    if(connect(dstfd,(sockaddr*)&addr,sizeof(addr))==-1)
        throw("Failed in connecting");
    if(write(clifd,&rep,sizeof(rep))!=sizeof(rep))throw("Failed to reply");
    thread c_d(proxy,clifd,dstfd);
    thread d_c(proxy,dstfd,clifd);
    c_d.join();
    d_c.join();
    exit(EXIT_SUCCESS);
}
void processbind(int clifd){
    reply rep;
    rep.vn=0;
    rep.cd=90;
    rep.dstip=0;
    int dstfd=setuplistensocket(0);
    sockaddr_in addr;
    memset(&addr,0,sizeof(addr));
    socklen_t clilen=sizeof(addr);
    getsockname(dstfd,(sockaddr*)&addr,&clilen);
    rep.dstport=addr.sin_port;
    if(write(clifd,&rep,sizeof(rep))!=sizeof(rep))throw("Failed to reply");
    int newfd=accept(dstfd,nullptr,nullptr);
    if(write(clifd,&rep,sizeof(rep))!=sizeof(rep))throw("Failed to reply");
    thread c_d(proxy,clifd,newfd);
    thread d_c(proxy,newfd,clifd);
    c_d.join();
    d_c.join();
    exit(EXIT_SUCCESS);
}

void senddeny(const request& req,int fd){
    reply rep;
    rep.vn=0;
    rep.cd=91;
    rep.dstip=0;
    rep.dstport=req.dstport;
    if(write(fd,&rep,sizeof(rep))!=sizeof(rep))throw("Failed to reply");
}

bool allowed(uint32_t ip){
    return (ip&allowmask) == allowid;
}
bool cli_allowed(uint32_t ip){
    return (ip&cli_allowmask) == cli_allowid;
}

void processsocks(int fd,uint32_t s_addr){
    try{
        signal(SIGINT,[](int){
            shutdown(STDIN_FILENO,SHUT_RDWR);
            shutdown(STDOUT_FILENO,SHUT_RDWR);
            exit(EXIT_SUCCESS);
        });
        auto rsize = read(fd,readbuf,readbuf_len);
        if(rsize==-1)throw("Failed while reading request");
        request *_req = (request*)(readbuf);
        request &req = *_req;
        auto usize = strnlen(req.userid,readbuf_len-sizeof(request)+1);
        if(usize==readbuf_len-sizeof(request)+1)throw("Malformat(Non terminate) request");
        if(req.vn!=4)throw("Not v4");
        bool v4a = ( req.dstip << 8 ) == 0 && (req.dstip >> 24) != 0;
        cerr << "vsersion: " << (unsigned int)req.vn << " ";
        cerr << "command: " << (unsigned int)req.cd << " ";
        cerr << "dstIP: " << inet_ntoa(*(in_addr*)(&req.dstip)) << " ";
        cerr << "dstPort: " << ntohs(req.dstport) << " ";
        cerr << "userid: " << req.userid << " ";
        if(v4a){
            cerr << "v4a" << endl;
            char* hostname = req.userid+usize+1;
            vector<char> hostbuf(readbuf_len);
            if(usize+sizeof(request)==(size_t)rsize){
                auto hsize = read(fd,hostbuf.data(),hostbuf.size());
                if(hsize<=0)throw("Can't read hostname");
                hostname=hostbuf.data();
                if(strnlen(hostname,readbuf_len)==readbuf_len)throw("Malformat(Non terminate) hostname");
            }
            auto hsize = strnlen(hostname,readbuf_len-sizeof(request)-usize);
            if(hsize==readbuf_len-sizeof(request)-usize)throw("Malformat(Non terminate) v4a request");
            cerr << "Hostname:" << hostname << endl;
            auto host = gethostbyname(hostname);
            if(host==nullptr)throw("Failed in gethostbyname");
            req.dstip = *((uint32_t*)host->h_addr_list[0]);
            cerr << "Resolved dstIP: " << inet_ntoa(*(in_addr*)(&req.dstip)) << " ";
        }
        cerr << endl;
        if(!cli_allowed(s_addr)){
            cerr << "Rejected(srcip not allowed)" << endl;
            senddeny(req,fd);
        }
        else if(!allowed(req.dstip)){
            cerr << "Rejected(dstip not allowed)" << endl;
            senddeny(req,fd);
        }
        else if(req.cd==1){
            cerr << "Connect Access Granted(dstip allowed)" << endl;
            processconnect(req,fd);
        }
        else{
            cerr << "Bind Access Granted(dstip allowed)" << endl;
            processbind(fd);
        }
    }
    catch (const char* err){
        cerr << "Error: " << err << endl;
        exit(EXIT_FAILURE);
    }
    //Prevent it doesnt exit
    exit(EXIT_FAILURE);
}

void readconf(){
    ifstream fin("socks.conf");
    string in;
    allowid=0;
    allowmask=0;
    while(getline(fin,in,'.')){
        allowid<<=8;
        allowmask<<=8;
        if(in[0]=='*'){
        }
        else{
            allowid+=stoi(in);
            allowmask+=255;
        }
    }
    allowid=htonl(allowid);
    allowmask=htonl(allowmask);
    cerr << "Allow ID: " << inet_ntoa(*(in_addr*)(&allowid)) << endl;
    cerr << "Allow Mask: " << inet_ntoa(*(in_addr*)(&allowmask)) << endl;
    fin=ifstream("client_socks.conf");
    cli_allowid=0;
    cli_allowmask=0;
    while(getline(fin,in,'.')){
        cli_allowid<<=8;
        cli_allowmask<<=8;
        if(in[0]=='*'){
        }
        else{
            cli_allowid+=stoi(in);
            cli_allowmask+=255;
        }
    }
    cli_allowid=htonl(cli_allowid);
    cli_allowmask=htonl(cli_allowmask);
    cerr << "Allow Client ID: " << inet_ntoa(*(in_addr*)(&cli_allowid)) << endl;
    cerr << "Allow Client Mask: " << inet_ntoa(*(in_addr*)(&cli_allowmask)) << endl;
}

int main (){
    try{
        signal(SIGCHLD,SIG_IGN);
        signal(SIGINT,[](int){
            shutdown(listenfd,SHUT_RDWR);
            exit(EXIT_SUCCESS);
        });
        readconf();
        listenfd = setuplistensocket(8790);
        while(1){
            sockaddr_in addr;
            memset(&addr,0,sizeof(sockaddr_in));
            socklen_t clilen=sizeof(sockaddr_in);
            int newfd=accept(listenfd,(sockaddr*)&addr,&clilen);
            if(newfd==-1)throw("Failed to accept new client");
            cerr << "New connect with fd: " << newfd << " From:" << inet_ntoa(addr.sin_addr) << ":" << ntohs(addr.sin_port) << endl;
            pid_t pid=fork();
            if(pid==-1)throw("Failed to fork");
            if(pid==0){
                close(listenfd);
                processsocks(newfd,addr.sin_addr.s_addr);
            }
            else{
                close(newfd);
            }
        }
    }
    catch(const char* err){
        cerr << errno << endl << err << endl;
    }
}
